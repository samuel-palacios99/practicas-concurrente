package unam.ciencias.computoconcurrente;
import java.lang.Math;

public class PrimeNumberCalculator implements Runnable{
    private int threads;
    private static int numPrimo;
    public static boolean result;
    public static int longitudSubInter; //Dividimos el intervalo [2,N-1] en this.threads cantidad de sub interbalos, uno por cada hilo

    public PrimeNumberCalculator() {
        this.threads = 1;
    }
    public PrimeNumberCalculator(int threads) {
        this.threads = threads > 1 ? threads : 1;
    }

    public boolean isPrime(int n) throws InterruptedException{
        this.numPrimo = n;
        this.result = true;
        //Dividimos el intervalo
        longitudSubInter = Math.round(Math.round(Math.sqrt(n)+1) / this.threads); 
        //Caso para el cual el numero primo sea 0 o 1
        if(n == 0 || n == 1){
            result = false;
            return result;
        } 
        //Usamos un for el cual nos ayudara a inicializar los threads
        for (int i = 0; i < this.threads; i++) {
            Thread thread = new Thread(new PrimeNumberCalculator());
            thread.start();
            thread.join();    
        } return result;
    }
    @Override
    public void run(){
        String nameThread = Thread.currentThread().getName();
        int startThread = Integer.valueOf(nameThread);
        int rango = startThread + longitudSubInter;
        if (rango > Math.round(Math.sqrt(this.+1)) ) {
            for (int i = startThread; i < numPrimo; i++) {
                if (i != 1 && numPrimo % i == 0) {
                    result = false;
                    return;
                }
            }
        }else{
            for (int i = startThread; i < rango; i++) {
                if (numPrimo % i == 0 && i != 1) {
                    result = false;
                    return;
                }
            }
        }
    }
}